## Keep it Clean

# Why Bad Data Ruins Projects and How to Fix it

![](./images/large_logo.png)

???

---

## How Bad Data Affects Results

![](img/bad_data.jpg)

???

Let's start by investigating some high profile examples of how bad data can destroy some Data Science projects...

---

![](img/tay.png)

---

![](img/babylon_app.png)

---

> The AI system has been put through rigorous testing that took place in collaboration with the U.K.'s Royal College of Physicians, as well as researchers from Stanford University and the Yale New Haven Health System.

<div class="footnote">
<p>
Aristos Georgiou On 6/27/18 at 5:21 PM. 2018. “This Artificial Intelligence Platform Can Provide Health Advice That Is as Accurate as a Real Doctor’s.” Newsweek. June 27, 2018. https://www.newsweek.com/ai-can-provide-health-advice-which-good-real-doctors-998461.
</p>
</div>

---

> Part of this testing involved the AI taking a medical diagnosis exam that trainee primary care physicians in the U.K. must pass to be able to practice independently. Remarkably, the AI doctor scored 81 percent on its first attempt. The average pass mark over the past five years for real doctors was 72 percent.

---

> further tests that mimic real-life scenarios were also conducted...

> And when tested only on common conditions, the AI’s accuracy jumped to 98 percent, compared with a range of 52 percent to 99 percent for the real physicians.

---

![](img/babylon_tweet.png)

<div class="footnote">
<p>
https://twitter.com/DrMurphy11/status/1118618977742274560
</p>
</div>

---

![](img/babylon_news.png)

<div class="footnote">
<p>
“Babylon Health Erases AI Test Event for Its Chatbot Doctor.” 2019. AI News (blog). April 12, 2019. https://www.artificialintelligence-news.com/2019/04/12/babylon-health-ai-test-gp-at-hand/.
</p>
</div>

---

![](img/babylon_after_remove.jpg)

---

<img width="75%" src="img/w2v_biases.png"></img>

<div class="footnote">
<p>
Swinger, Nathaniel, Maria De-Arteaga, Neil Thomas Heffernan IV, Mark DM Leiserson, and Adam Tauman Kalai. 2018. “What Are the Biases in My Word Embedding?” ArXiv:1812.08769 [Cs], December. http://arxiv.org/abs/1812.08769.
</p>
</div>

???

Columns represent name groups, rows represent clustered categories (e.g., a cluster of food-related words).

Orange indicate associations where the crowd’s most commonly chosen name group agrees with that of that generated.

---

[Google Translate](https://translate.google.co.uk/#view=home&op=translate&sl=en&tl=tr&text=He%20is%20a%20nurse%0AShe%20is%20a%20software%20developer)

???

As a more practical example, let's look at Google translate.

---

![](img/katecrawford_tweet.png)

<div class="footnote">
<p>
https://twitter.com/katecrawford/status/1173666732923396098
</p>
</div>

---

## A Guardian Reporter Finds...

<img width="75%" src="img/guardian.jpg"></img>

<div class="footnote">
<p>
https://www.theguardian.com/technology/2019/sep/17/imagenet-roulette-asian-racist-slur-selfie
</p>
</div>

---

![](img/adversarial.png)

<div class="footnote">
<p>
Qiu, Shilin, Qihe Liu, Shijie Zhou, and Chunjiang Wu. 2019. ‘Review of Artificial Intelligence Adversarial Attack and Defense Technologies’. Applied Sciences 9 (5): 909. https://doi.org/10.3390/app9050909.
</p>
</div>

???

Related to this is the increasing amount of research into adversarial attacks.

These are only possible because the attacks find regions within the model that produces incorrect results. This, once again, is usually due to the fact that there is not enough data to "fill in the gaps".

A related issue is that when you use a more and more complex model, you need greater data coverage to fill in the increasingly small gaps.

In simpler models we can provide constraints to make the model more robust. For more complex models we can use the Adversarial Attacks themselves to train the model to prevent attacks.

---

https://cloud.google.com/vision/docs/drag-and-drop

<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

<div class="container">

<div class="col">
<img src="img/m.jpg"></img>
</div>

<div class="col">
<img src="img/v.jpg"></img>
</div>

</div>

 

???

Another practical example, let's look at the Google Vision API...

This highlights the complexity of image tagging. You need to have many examples of everything in order to have full coverage. Since there's only one Mona Lisa, that's not much to train upon.

---

![](img/costs.jpg)

<div class="footnote">
<ul>
<li>
https://www.ibmbigdatahub.com/infographic/four-vs-big-data
</li>
<li>
https://hbr.org/2016/09/bad-data-costs-the-u-s-3-trillion-per-year
</li>
<li>
Gartner, Dirty data is a business problem, not an IT problem, 2007, now removed
</li>
</ul>
</div>

???

More generally, bad data has a large bottom-line financial impact on a business...

---

![](img/survey_results.png)

<div class="footnote">
<p>
Loten, Angus. 2019. AI Efforts at Large Companies May Be Hindered by Poor Quality Data. Wall Street Journal, March 4, 2019, sec. C Suite. https://www.wsj.com/articles/ai-efforts-at-large-companies-may-be-hindered-by-poor-quality-data-11551741634.
</p>
</div>

???

In a 2019 PwC Survey, 300 executives from companies with revenues over \$500 million were surveyed to find out how bad data is affecting their businesses by PwC (Loten 2019).

The report finds that cleaning their data will lead to average cost savings of 33% while boosting revenues by an average of 31%.

“Data can, for example, enable retailers to target promotions and products to individual consumers, as well to deploy AI to game out new go-to-market strategies,” researchers said in the report.

---

> ### Bad Data introduces an extraordinary amount of technical debt

???

Considering these reports span nearly a decade, it looks like this trend is set to continue.

If we consider how quickly data is being amassed and how much technical debt is introduced by not dealing with it upon entry, I think it will be hard to ever reduce these numbers.

...

So now we know how bad data affects businesses, a good Data Scientist should ask, why?

---

## Why Bad Data Affects Results

- **Deduction**: Newton
- **Induction**: Sherlock Holmes

???

One of the general reasons why this happens is the due to the way that we perform data science.

Deduction is the process of developing a hypothesis, then using data to prove or disprove that hypothesis. This is the scientific method.

Most modern data science is performed the opposite way around. We use the data to build a model to produce a hypothesis, then we test that result against an expected hypothesis.

Newton used deduction to develop the laws of relativity.

Sherlock Holmes uses induction to solve crimes.

So what we are doing is developing a working model from given set of data. Very, very often, that data itself is biased (parole model, amazon model) or corrupted (tay.ai). So any models that are produced from it are also biased and corrupted.

Which leads me to...

---

![](img/garbage.jpg)

???

The well know adage of garbage in, garbage out. If we translate that into English, that means rubbish in, rubbish out.

If you base your models on bad data then it will produce bad models.

---

![](img/noise_assume.jpg)

???

Another related issue, that you can't completely avoid, is that most data is affected by noise. Noise is simply parts of the data that doesn't help you solve your problem.

If we build a model around a dataset that is full of noise, then we end up building a model of the noise, not of the underlying data that we are interested in.

We have to be careful to suppress or remove the noise as much as practicable. And we must train our models to average out the noise.

Assumptions about data often come back to haunt you too. We often make incorrect assumptions about the data, that are only obvious in hindsight.

We combat our assumptions through a combination of skepticism, data cleaning and automated tests.

---

## Group Question

### What is the deadliest animal in Australia?

---

![](images/snakes.png)

<div class="footnote">
<p>
https://www.bbc.co.uk/news/world-australia-38592390
</p>
</div>

???

From 2000 to 2013, horses were responsible for 74 deaths.

Wasps, bees, hornets 27 (25 bees, 2 wasps)
Snakes 27
Spiders 0
Ticks and ants 5
Marine animals 3 (box jellyfish)
Centipedes/ millipedes 0
Scorpions 0
Unknown animal or plant 2

However, that's not to say that spiders aren't dangerous...

---

> **21-year-old Australian tradesman has been bitten by a venomous spider on the penis for a second time.**

> Jordan, who preferred not to reveal his surname, said he was bitten on "pretty much the same spot" by the spider.

> "I'm the most unlucky guy in the country at the moment," he told the BBC

<div class="footnote">
<p>
https://www.bbc.co.uk/news/world-australia-37481251
</p>
</div>

???

"I was sitting on the toilet doing my business and just felt the sting that I felt the first time.

"I was like 'I can't believe it's happened again.' I looked down and I've seen a few little legs come from around the rim."

...

"They got worried the first time," he said.

"This time they were making jokes before I was getting in the car."

---

## Visualising Data

- Always visualise your data
- How?
  - Histogram
  - Scatter plot (matrix)
  - Segmented (faceted) bar chart
  - Nullity plot
  - Correlation plot

???

The human brain has evolved to be very good at consuming large amounts of visual or audio data. Being able to see or hear that tiger has an evolutionary advantage!

We can take advantage of our faculties to efficiently scan data for things that we know will decrease model performance (or break it!).

Hence, always visualise your data.

Over the next few slides we will take a look at a few ways of visualising your data in a generic way.

There are also many domain specific ways in which you might visualise your data. Seek them out and use them to your benefit.

---

## Simple Models

![](img/iris_svm_kernel.png)

---

## Data Leakage

Very easy to accidentally include future data in training data.

- Oversampling
- Running dimensionality reduction on the _whole_ dataset
- Preprocessing over the _whole_ dataset
- Including a feature that is only populated _after_ the label has been applied

---

## Missing data - Important?

- Missing data doesn't necessarily mean `numpy.nan`!

```python
>>> print(titanic.count())
pclass       1309
survived     1309
name         1309
sex          1309
age          1046
sibsp        1309
parch        1309
ticket       1309
fare         1308
cabin         295
embarked     1307
boat          486
body          121
home.dest     745
dtype: int64
```

???

This is the Titanic dataset again. Note that there are only 1046 people with ages. That doesn't mean that the age for that person is NaN. They undoubtedly have an age.

We could attempt to fix some of the missing ages using techniques shown in the next few slides.

However, the feature called "boat" is the lifeboat identifier. There weren't enough lifeboats for everyone. So people would only have a lifeboat number if they were actually able to get on a lifeboat. This is probably a good predictor of survival.

In other words, missing data can have different meanings. Sometimes it can actually help the problem. It all depends on the context.

---

## Fixing missing data

- Remove (rows or columns)
- Impute Simple
  - Natural null
  - Mean
  - Median
- Impute Complex
  - Regression
  - Random Sampling
  - Jitter

---

## Noise: What is noise?

Weeds are just flowers that you don't like. Noise is data that you don't like.

![](./images/weeds.jpg)

???

Example from Instacart:
When a shopper finishes a delivery, he has to press a button in the shopper app to end the trip.

Then we use these timestamps to train our delivery time prediction model.

However, for various reasons, some shoppers may not click “delivered” exactly when they actually do it, so we built some models to filter and infer the right timestamps from GPS data.

---

### Noise: Types of Noise

- Class
- Feature (column)
- Observation (row)

![](./images/countries.png)

---

### Noise: Improving Noise

- Aggregation
  - Average (stacking/beamforming/radon transform)
  - Median (popcorn noise)
- Simple modelling
  - Smoothing
  - Normalisation
- Complex modelling
  - Regression or fitting
- Dimensionality Reduction and Restoration
  - Transformations (FFT, Wavelet)
  - Encoding/Embedding (Autoencoder, NLP Embeddings)

???

- **Bad news:** As much as you keep picking those weeds, they keep reappearing.

All of these attempt to throw away "bad" parts of the signal. The key thing to realise is that "good" and "bad" depend on the problem at hand. They depend on perspective.

Our perception of what is right or wrong can vary by the minute, year or lifetime.

Noise reduction techniques don't actually reduce the noise. They just throw away things you don't want. Just like when you're weeding in the garden.

---

## Anomalies (a.k.a. outliers)

> Data that is not expected (in a statistical sense)

![](./images/outlier.jpg)

---

### Anomaly Types

- Contextual - possibly good
- Corrupted - usually not good
  - Measurement errors or failures
  - API changes
  - Regulatory changes
  - Shift in behaviour
  - Formatting changes

???

- Measurement errors or failures
  Sensor failure
- API changes
  Yahoo being bought out and closing down all the finance APIs
- Regulatory changes
  GDPR
- Shift in behaviour
  Ecommerce site
- Formatting changes
  Change in date format

---

### Detecting Anomalies

_a large field in its own right_

1. Define what is normal (through a model)
2. Set a threshold to define "not normal"

---

### Detecting Anomalies for Data Cleaning

1. Visualise your data!
2. Everything else
   1. Classification task
   2. Clustering
   3. Regression/fitting + thresholds

???

The first thing you should do every time, if you can, is visualise your data and investigate the data that is obviously bad.

More often than not you will learn something about the data which you will find useful.

If and only if you need to automate this task then look at automated methods. You should do this if this is critical to your product. This is a difficult task.

The main reason why it is difficult is that often you don't have any labels or ground truth. Are you really sure that it is an outlier? Or is that actually quite normal?

Most outlier detection techniques rely on attempting to build a model then checking to ensure that data fit within some statistical bounds of that model.

The model could be a classification one, if you're lucky enough to have labelled outliers.

It could be a regression task if your data is statistically robust.

Or it could be a clustering task if neither of these work.

---

### Example Regression Task - Wine Quality

![](./python/images/14_corrupted_data_wine_original.png)

---

![](./python/images/14_corrupted_data_wine_residuals.png)

---

![](./python/images/14_corrupted_data_wine_outliers_removed.png)

---

## Scale

![](python/images/30_scales_titanic_classification_scaled.png)

---

## Normality

![75%](./images/mse.jpg)

???

Normality is important because most algorithms make some assumption that your data is normally distributed.

Take regression for example. Often it is optimised by calculating the mean squared error.

What if you data isn't well represented by the mean?

Let's take a look back at the distributions again...

---

![:scale 50%](./images/distributions.png)

Look again at the parameters of all these distributions. Note how few of them use "mean".

The vast majority of data cannot be represented by a mean. And the algorithm will not work.

The best case...

---

![](./python/images/20_transforming_data_stars_regression.png)

---

### Fixing: Domain Knowledge

![](./python/images/20_transforming_data_wind_histogram.png)

---

![](./python/images/20_transforming_data_wind_histogram_sqrt.png)

---

![](./python/images/20_transforming_data_wind_histogram_fit.png)

---

![](./python/images/20_transforming_data_wind_sqrt_qqplot.png)

---

### Fixing: Arbitrary Functions

- We can use **any** mathematical function to transform our data\*

\*so long as it's invertible

---

![](./python/images/20_transforming_data_titanic_fare.png)

---

![](./python/images/20_transforming_data_titanic_qqplot_fare.png)

---

![](./python/images/20_transforming_data_titanic_fare_log10_hist.png)

---

![](./python/images/20_transforming_data_titanic_fare_log10_qqplot.png)

---

## Things I've Skipped Over

- Practical examples
- Windsorising
- Types of data
- Scaling
- Derived Data
- Box Cox transform
- Time series data
- Feature selection
- Dimensionality reduction
- Data integration
- Probably lots more!

???

That was a whistle-stop tour of some of the most important parts of data cleaning.

I want to re-emphasise the importance of visualisation and asking questions.

But there is also a lot of things I didn't go through. For example...

I provide training on all of these topics and more, with the help of many practical examples.

---

## Concluding Remarks

- Data Cleaning:

  - is important
  - is open to interpretation
  - is (arguably) a manual process
  - takes a lot of time (approx 60% of a Data Scientist's time)
  - requires domain knowledge

???

If you need anyone to help you with your data cleaning, then please come and speak to me.

With that, I'll end it there. I hope you've enjoyed it, and try not to get bitten by any spiders!

Thanks!

---

![](./images/large_logo.png)

Data Science **Training, Consultancy, Development**

![](img/twitter.png) [@DrPhilWinder](https://twitter.com/DrPhilWinder)

![](img/linkedin.png) [DrPhilWinder](https://www.linkedin.com/in/drphilwinder)

![](img/www.png) https://WinderResearch.com

![](img/email.png) [phil@WinderResearch.com](mailto:phil@WinderResearch.com)

---

## Bibliography

- Examples: https://www.reddit.com/r/MachineLearning/comments/965zgf/d_tell_me_about_how_you_were_a_victim_of_data/
- Book: Janert, P.K. Data Analysis with Open Source Tools: A Hands-On Guide for Programmers and Data Scientists. O’Reilly Media, 2010. https://amzn.to/2VFqOYx.
- Data Types in Statistics, Niklas Donges - https://towardsdatascience.com/data-types-in-statistics-347e152e8bee
- Quick intro to handling missing data: https://towardsdatascience.com/the-tale-of-missing-values-in-python-c96beb0e8a9d

---

- Pandas documentation on missing data: https://pandas.pydata.org/pandas-docs/stable/missing_data.html
- Bit more information about anomaly detection: https://towardsdatascience.com/a-note-about-finding-anomalies-f9cedee38f0b
- Good short free book on anomaly detection: Practical Machine Learning: A New Look at Anomaly Detection, Ted Dunning, Ellen Friedman, O'Reilly Media, Inc., 2014, ISBN 1491914181, 9781491914182
- Cool Library for benchmarking time series anomaly detection: https://github.com/numenta/NAB
- Nice run through of day-to-day problems with data: https://medium.com/@bertil_hatt/what-does-bad-data-look-like-91dc2a7bcb7a

---

- Short section on dealing with corrupted data - Raschka, S. Python Machine Learning. Packt Publishing, 2015. https://books.google.co.uk/books?id=GOVOCwAAQBAJ.
- Presentation on Seaborn Styles - https://s3.amazonaws.com/assets.datacamp.com/production/course_6919/slides/chapter2.pdf
- Code to fit all distributions: https://stackoverflow.com/questions/6620471/fitting-empirical-distribution-to-theoretical-ones-with-scipy-python
