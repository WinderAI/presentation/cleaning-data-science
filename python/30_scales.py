from pathlib import Path

import matplotlib
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import sklearn.svm
import numpy as np

dir_path = Path(__file__).parent
filename = str(Path(__file__).stem)

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 18

matplotlib.rc('font', size=SMALL_SIZE)          # controls default text sizes
matplotlib.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
matplotlib.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y
matplotlib.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
matplotlib.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

sns.set(style="white")
sns.set_context("talk")
sns.set_palette("cubehelix")

np.random.seed(0)


def savefig(data: str, name: str):
    image_filename = filename + "_" + data + "_" + name + ".png"
    image_path = dir_path / 'images' / image_filename
    print('Saving png to {}'.format(image_path))
    plt.gcf().set_figwidth(12)
    plt.gcf().set_figheight(6)
    plt.tight_layout()
    plt.savefig(str(image_path))
    plt.close()


titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))
titanic = titanic[['survived', 'fare', 'age']]
titanic.dropna(inplace=True)
titanic = titanic[(titanic[['fare', 'age']] != 0).all(
    axis=1)]  # Remove zero fares
y = titanic['survived']
X = titanic[['fare', 'age']]

clf = sklearn.svm.LinearSVC(tol=1e-2, max_iter=10).fit(X, y)
xx, yy = np.mgrid[0:600:1, 0:100:1]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

fig, (ax1, ax2) = plt.subplots(ncols=2)

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax1).set_title(
    "Titanic Survivors - SVM (10 iter) - {:.1f}%".format(score))
ax1.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)

X[:] = sklearn.preprocessing.MinMaxScaler().fit_transform(X[:])
clf = sklearn.svm.LinearSVC(tol=1e-2, max_iter=10).fit(X, y)
xx, yy = np.mgrid[0:1:0.001, 0:1:0.001]
grid = np.c_[xx.ravel(), yy.ravel()]
probs = clf.predict(grid).reshape(xx.shape)
score = clf.score(X, y) * 100

sns.scatterplot(data=X, x='fare', y='age', hue=y, linewidth=0, ax=ax2).set_title(
    "Titanic Survivors - SVM (10 iter, scaled) - {:.1f}%".format(score))
ax2.contour(xx, yy, probs, levels=[.5], cmap="Greys", vmin=0, vmax=.6)
savefig('titanic', 'classification_scaled')
