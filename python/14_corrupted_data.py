from sklearn.linear_model import LinearRegression
import missingno as msno
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from pathlib import Path
import pygal
from pandas.plotting import scatter_matrix
import numpy as np
import seaborn as sns
import statsmodels.api as sm
from scipy.stats.mstats import winsorize

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 18

matplotlib.rc('font', size=SMALL_SIZE)          # controls default text sizes
matplotlib.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
matplotlib.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y
matplotlib.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
matplotlib.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
matplotlib.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

sns.set(style="white")


def savefig(data: str, name: str):
    image_filename = filename + "_" + data + "_" + name + ".png"
    image_path = dir_path / 'images' / image_filename
    print('Saving png to {}'.format(image_path))
    plt.gcf().set_figwidth(12)
    plt.gcf().set_figheight(6)
    plt.tight_layout()
    plt.savefig(str(image_path))
    plt.close()


dir_path = Path(__file__).parent
filename = str(Path(__file__).stem)

titanic = pd.read_csv(str(dir_path / 'data' / 'original_titanic.csv'))

print(titanic.count())

# Windsorizing

starsCYG = sm.datasets.get_rdataset("starsCYG", "robustbase", cache=True).data
limit = 0.15
winsorized_x = starsCYG.copy()
winsorized_x['log.Te'] = winsorize(starsCYG['log.Te'], limits=limit)


# Overwrite to get matching colours
fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=True)
sns.scatterplot(data=starsCYG, x='log.Te', y='log.light', linewidth=0, ax=ax1).set_title(
    "Star Cluster CYG OB1 - Original")
sns.scatterplot(data=winsorized_x, x='log.Te', y='log.light', linewidth=0, ax=ax1).set_title(
    "Star Cluster CYG OB1 - Original")
sns.scatterplot(data=winsorized_x, x='log.Te', y='log.light', linewidth=0, ax=ax2).set_title(
    "Star Cluster CYG OB1 - Windsorized")
sns.scatterplot(data=winsorized_x, x='log.Te', y='log.light', linewidth=0, ax=ax2).set_title(
    "Star Cluster CYG OB1 - Windsorized")
savefig('stars', 'windsorized')

# Anomaly

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv"
wine = pd.read_csv(url)
print(wine.head(n=5))
wine = pd.read_csv(url, sep=";")
print(wine.head(n=5))
print("Shape of Red Wine dataset: {s}".format(s=wine.shape))
print("Column headers/names: {s}".format(s=list(wine)))
print(wine.info())
sns.regplot(data=wine, x='alcohol', y='quality').set_title(
    "Wine Quality")
savefig('wine', 'original')


mdl = LinearRegression().fit(wine[['alcohol']], wine['quality'])
residuals = wine['quality'] - mdl.predict(wine[['alcohol']])
residuals = residuals / np.std(residuals)
ax = sns.distplot(residuals, kde=False, norm_hist=False)
ax.set_title("Wine Quality Residuals")
ax.set_xlabel("Standard Deviations")
savefig('wine', 'residuals')

std_dev = np.std(residuals)
wine_outliers = wine.loc[np.abs(residuals) > 3]
wine_inliers = wine.loc[np.abs(residuals) <= 3]
fig, ax1 = plt.subplots(ncols=1)
sns.regplot(data=wine_inliers, x='alcohol', y='quality',
            ax=ax1).set_title("Wine Quality - Outiliers Removed (+/- 3 s.d.)")
sns.scatterplot(data=wine_inliers, x='alcohol',
                y='quality', linewidth=0, ax=ax1)
sns.scatterplot(data=wine_outliers, x='alcohol',
                y='quality', linewidth=0, ax=ax1)
savefig('wine', 'outliers_removed')
